import React, { useState, useEffect } from 'react';
import './App.scss';
import Button from './component/Button';

import ThemeSwitch from './component/ThemeSwitch';

const KEYPADSTYLE = {
  dark: {
    numeric: 'dark--grey',
    topRow: 'grey',
  },
  light: {
    numeric: 'white',
    topRow: 'light--grey',
  },
}

function App() {
  const [theme, setTheme] = useState('dark');
  const [calcFontSize, setCalcFontSize] = useState(6);

  const [prevNumber, setPrevNumber] = useState(0);
  const [currNumber, setCurrNumber] = useState(0);
  const [operation, setOperation] = useState('');
  const [result, setResult] = useState(null);

  const formatNumber = (value) => {
    return value.toLocaleString('id-ID', { maximumFractionDigits: 8 });
  }

  const displayOperation = () => {
    if (result !== null) return `${formatNumber(prevNumber)} ${formatNumber(operation)} ${formatNumber(currNumber)}`;
    if (operation !== '') return `${formatNumber(prevNumber)} ${formatNumber(operation)}`;
  }

  const displayCalcNumber = () => {
    if (result !== null) return formatNumber(result);
    if (currNumber === 0) return 0;

    return formatNumber(currNumber);
  }

  const inputNumber = (value) => {
    // prevent repeated comma
    if (value === ',' && currNumber.toString().includes(',')) return;

    if (currNumber.toString().length < 9) {
      const input = currNumber.toString() + value;
      
      if (value === ',') return setCurrNumber(input);     
      setCurrNumber(parseFloat(input.replace(',', '.')));
    }
  }

  const handleOperation = (operationType) => {
    if (prevNumber === 0 && currNumber === 0) return;
    
    if (prevNumber === 0) {
      setOperation(operationType);
      setPrevNumber(currNumber);
      setCurrNumber(0);
    } else if (prevNumber !== 0 && currNumber === 0) {
      setOperation(operationType);
    } else {
      calcResult();
      setResult(null);
      setOperation(operationType);
      setPrevNumber(basicOperation());
      setCurrNumber(0);
    }
  }

  const clear = () => {
    setPrevNumber(0);
    setCurrNumber(0);
    setOperation('');
    setResult(null);
  }

  const deleteInput = () => {
    if (result !== null) return;
    if ((currNumber.toString()[0] === '-' && currNumber.toString().length <= 2) || currNumber.toString().length <= 1) return setCurrNumber(0);
    return setCurrNumber(currNumber.toString().slice(0, -1));
  }

  const plusMinus = () => {
    if (result !== null) return setResult(result * -1);
    return setCurrNumber(currNumber * -1);
  }

  const percentage = () => {
    if (result !== null) return setResult(result / 100);
    return setCurrNumber(currNumber / 100);
  }

  const basicOperation = () => {
    switch(operation) {
      case '+':
        return prevNumber + currNumber;
      case '-':
        return prevNumber - currNumber;
      case '÷':
        return prevNumber / currNumber;
      case 'x':
        return prevNumber * currNumber;
      default:
        return null;
    }
  }

  const calcResult = () => {
    setResult(basicOperation());
  }

  useEffect(() => {
    let calcNum = result || currNumber;
    let calcLength = calcNum.toString().replace('.', '').length;

    // adaptive font size
    if (calcLength >= 10) {
      setCalcFontSize(3.7);
    } else if (calcLength === 9) {
      setCalcFontSize(4);
    } else if (calcLength === 8) {
      setCalcFontSize(4.5);
    } else if (calcLength === 7) {
      setCalcFontSize(5);
    } else {
      setCalcFontSize(6);
    }

  }, [result, currNumber]);

  return (
      <div className={`App ${theme}`}>
        <div id="ribbon">
          <h3>
            {process.env.NODE_ENV.toUpperCase()}
          </h3>
        </div>
        <div className="main-container">
          <div className="theme-switch">
            <ThemeSwitch theme={theme} setTheme={setTheme} />
          </div>
          <section className="calculation-wrapper">
            <h2 className="operation">{displayOperation()}</h2>
            <h1 className="result" style={{ fontSize: `${calcFontSize}rem` }}>{displayCalcNumber()}</h1>
          </section>
          <section className="keypad-wrapper">
              <Button text={'C'} baseColor={KEYPADSTYLE[theme].topRow} onClick={clear}/>
              <Button icon={'plus-minus'} baseColor={KEYPADSTYLE[theme].topRow} onClick={plusMinus}/>
              <Button text={'%'} baseColor={KEYPADSTYLE[theme].topRow} onClick={percentage}/>
              <Button text={'÷'} baseColor={'blue'} onClick={() => handleOperation('÷')}/>
              <Button text={'7'} baseColor={KEYPADSTYLE[theme].numeric} onClick={() => inputNumber(7)}/>
              <Button text={'8'} baseColor={KEYPADSTYLE[theme].numeric} onClick={() => inputNumber(8)}/>
              <Button text={'9'} baseColor={KEYPADSTYLE[theme].numeric} onClick={() => inputNumber(9)}/>
              <Button text={'×'} baseColor={'blue'} onClick={() => handleOperation('x')}/>
              <Button text={'4'} baseColor={KEYPADSTYLE[theme].numeric} onClick={() => inputNumber(4)}/>
              <Button text={'5'} baseColor={KEYPADSTYLE[theme].numeric} onClick={() => inputNumber(5)}/>
              <Button text={'6'} baseColor={KEYPADSTYLE[theme].numeric} onClick={() => inputNumber(6)}/>
              <Button text={'-'} baseColor={'blue'} onClick={() => handleOperation('-')}/>
              <Button text={'1'} baseColor={KEYPADSTYLE[theme].numeric} onClick={() => inputNumber(1)}/>
              <Button text={'2'} baseColor={KEYPADSTYLE[theme].numeric} onClick={() => inputNumber(2)}/>
              <Button text={'3'} baseColor={KEYPADSTYLE[theme].numeric} onClick={() => inputNumber(3)}/>
              <Button text={'+'} baseColor={'blue'} onClick={() => handleOperation('+')}/>
              <Button text={','} baseColor={KEYPADSTYLE[theme].numeric} onClick={() => inputNumber(',')}/>
              <Button text={'0'} baseColor={KEYPADSTYLE[theme].numeric} onClick={() => inputNumber(0)}/>
              <Button icon={'delete'} baseColor={KEYPADSTYLE[theme].numeric} onClick={deleteInput}/>
              <Button text={'='} baseColor={'blue'} onClick={calcResult}/>
          </section>
        </div>
      </div>
  );
}

export default App;
