import React from "react";
import "../styles/Button.scss";

const FONTCOLOR = {
    "grey": 'light',
    "dark--grey": 'light',
    "light--grey": 'dark',
    "white": 'dark',
    "blue": 'light'
};

const Button = ({ text, icon, onClick, baseColor }) => {
  return (
    <button
      id="button-container"
      type="button"
      className={`${baseColor}`}
      onClick={onClick}
    >
      {text && <h2>{text}</h2>}
      {icon && <Icon color={FONTCOLOR[baseColor]} type={icon} />}
    </button>
  );
};

const Icon = ({ color, type }) => {
  const fill = color === 'dark'? 'black' : 'white';

  const pickIcon = {
    'delete': <path fillRule="evenodd" clipRule="evenodd" d="M11.0858 7H11.5H27.5H28.5V8V24V25H27.5H11.5H11.0858L10.7929 24.7071L2.79292 16.7071L2.08582 16L2.79292 15.2929L10.7929 7.29289L11.0858 7ZM11.9142 9L4.91424 16L11.9142 23H26.5V9H11.9142ZM15.5 11.5858L16.2071 12.2929L18.5 14.5858L20.7929 12.2929L21.5 11.5858L22.9142 13L22.2071 13.7071L19.9142 16L22.2071 18.2929L22.9142 19L21.5 20.4142L20.7929 19.7071L18.5 17.4142L16.2071 19.7071L15.5 20.4142L14.0858 19L14.7929 18.2929L17.0858 16L14.7929 13.7071L14.0858 13L15.5 11.5858Z" fill={fill} />,
    'plus-minus': <path fillRule="evenodd" clipRule="evenodd" d="M23.5821 7.55469L24.1367 6.72264L22.4726 5.61324L21.9179 6.44529L9.91795 24.4453L9.36325 25.2773L11.0273 26.3867L11.5821 25.5547L23.5821 7.55469ZM9.75 5.99999V6.99999V8.99999H11.75H12.75V11H11.75H9.75V13V14H7.75V13V11H5.75H4.75V8.99999H5.75H7.75V6.99999V5.99999H9.75ZM21.75 21H20.75V23H21.75H27.75H28.75V21H27.75H21.75Z" fill={fill}/>
  }

  return (
    <svg width="33" height="32" viewBox="0 0 33 32" fill="none" xmlns="http://www.w3.org/2000/svg" id="icon-container">
      {pickIcon[type]}
    </svg>
  );
}

export default Button;
