import React from 'react';
import '../styles/ThemeSwitch.scss';

const ThemeSwitch = ({ theme, setTheme }) => {

  const changeTheme = () => {
    setTheme(theme === 'dark' ? 'light' : 'dark');
    console.log(theme)
  }

  return (
    <label id="theme-switch">
        <input type="checkbox" onChange={changeTheme} checked={theme === 'light'} />
        <span className="slider">
          <div className="round-thumb"></div>
        </span>
    </label>
  )
}

export default ThemeSwitch;