module.exports = {
  local: 'http://localhost:3000/',
  prod: 'https://calculator-env-production.herokuapp.com/',
  dev: 'https://calculator-env-development.herokuapp.com/'
}